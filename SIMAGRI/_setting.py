# -*- coding: utf-8 -*-

from SIMAGRI.uivar.scenariossetup import UIVarScenariosSetup
from SIMAGRI.uivar.simulationsetup import UIVarSimulationSetup
# from SIMAGRI.uivar.temporaldownscaling import UIVarTemporalDownscaling
from SIMAGRI.uivar.dssatsetup1 import UIVarDSSATSetup1
from SIMAGRI.uivar.dssatsetup2 import UIVarDSSATSetup2
# from SIMAGRI.uivar.diseasesetup  import UIVarDISEASESetup

class Setting(object):

  # SimulationSetup = UIVarSimulationSetup() #no need for historical run
  # TemporalDownscaling = UIVarTemporalDownscaling()
  DSSATSetup1 = UIVarDSSATSetup1()
  DSSATSetup2 = UIVarDSSATSetup2()
  # DISEASESetup = UIVarDISEASESetup()
  ScenariosSetup = UIVarScenariosSetup()

  def __init__(self):
    pass

  def loadConfig(self):
    # self.SimulationSetup.loadConfig()  #no need for historical run
    # self.TemporalDownscaling.loadConfig()
    self.DSSATSetup1.loadConfig()
    self.DSSATSetup2.loadConfig()
    # self.DISEASESetup.loadConfig()
    self.ScenariosSetup.loadConfig()

  def saveConfig(self):
    # self.SimulationSetup.saveConfig()  #no need for historical run
    # self.TemporalDownscaling.saveConfig()
    self.DSSATSetup1.saveConfig()
    self.DSSATSetup2.saveConfig()
    # self.DISEASESetup.saveConfig()
    self.ScenariosSetup.saveConfig()