# -*- coding: utf-8 -*-
"""
Created on Thu Jun 15 11:53:54 2017

@author: geoslegend
"""

import os
import sys
import csv

# http://ourcstory.tistory.com/48
def get_csv_reader(filename, delimiter=','):
  reader = [] 
  if not os.path.isfile(filename): 
    pass
  else: 
    csvfile = open(filename, "rb") 
    reader = csv.DictReader(csvfile, delimiter=delimiter) # csv.reader
  return list(reader)


dir = 'F:/Dropbox/2017_Co_Research/03Phi/CAMDT_2_beta1/data/Disease'

LEGA = 'DISEASE_result_LEGA.csv'

a = get_csv_reader(os.path.join(dir, LEGA))

