# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._configuration import *

class UIVarScenariosSetup():

  _Section_Name = 'ScenariosSetup'   ## _single_leading_underscore => private variable

  # Working directory
  Working_directory = None # Wdir_path

  # # Threshold for water stress index
  # WSI_threshold = None # wsi_threshold

  # What-If scenarios
  WIfS_name1 = None # name1
  WIfS_crop1 = None # MZ or BN
  WIfS_crop_price1 = None  # price1
  WIfS_cost_Nfert1 = None  # costN1
  WIfS_cost_irr1 = None  # costi1
  WIfS_general_cost1 = None  # costG1
  WIfS_comment1 = None # comment1

  WIfS_name2 = None # name2
  WIfS_crop2 = None # MZ or BN
  WIfS_crop_price2 = None  # price2
  WIfS_cost_Nfert2 = None  # costN2
  WIfS_cost_irr2 = None  # costI2
  WIfS_general_cost2 = None  # costG2
  WIfS_comment2 = None # comment2

  WIfS_name3 = None # name3
  WIfS_crop3 = None # MZ or BN
  WIfS_crop_price3 = None  # price3
  WIfS_cost_Nfert3 = None  # costN3
  WIfS_cost_irr3 = None  # costI3
  WIfS_general_cost3 = None  # costG3
  WIfS_comment3 = None # comment3

  WIfS_name4 = None # name4
  WIfS_crop4 = None # MZ or BN
  WIfS_crop_price4 = None  # price4
  WIfS_cost_Nfert4 = None  # costN4
  WIfS_cost_irr4 = None  # costI4
  WIfS_general_cost4 = None  # costG4
  WIfS_comment4 = None # comment4

  WIfS_name5 = None # name5
  WIfS_crop5 = None # MZ or BN
  WIfS_crop_price5 = None  # price5
  WIfS_cost_Nfert5 = None  # costN5
  WIfS_cost_irr5 = None  # costI5
  WIfS_general_cost5 = None  # costG5
  WIfS_comment5 = None # comment5


  def __init__(self):
    pass

  def initVars(self):
    pass

  def loadConfig(self):
    # Working directory
    self.Working_directory = get_config_value_into_string(self._Section_Name, 'Working_directory') # Wdir_path

    # # Threshold for water stress index
    # get_config_value_into_pmw_entryfield(self._Section_Name, 'WSI_threshold', self.WSI_threshold)   # wsi_threshold

    # What-If scenarios
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_name1', self.WIfS_name1)  # name1
    get_config_value_into_tkinter_label(self._Section_Name, 'WIfS_crop1', self.WIfS_crop1)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_crop_price1', self.WIfS_crop_price1) # price1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert1', self.WIfS_cost_Nfert1) # costN1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr1', self.WIfS_cost_irr1) # costi1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_general_cost1', self.WIfS_general_cost1) # costG1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_comment1', self.WIfS_comment1) # comment1

    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_name2', self.WIfS_name2)  # name2
    get_config_value_into_tkinter_label(self._Section_Name, 'WIfS_crop2', self.WIfS_crop2)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_crop_price2', self.WIfS_crop_price2) # price2
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert2', self.WIfS_cost_Nfert2) # costN2
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr2', self.WIfS_cost_irr2) # costi2
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_general_cost2', self.WIfS_general_cost2) # costG2
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_comment2', self.WIfS_comment2) # comment2

    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_name3', self.WIfS_name3)  # name3
    get_config_value_into_tkinter_label(self._Section_Name, 'WIfS_crop3', self.WIfS_crop3)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_crop_price3', self.WIfS_crop_price3) # price3
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert3', self.WIfS_cost_Nfert3) # costN3
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr3', self.WIfS_cost_irr3) # costi3
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_general_cost3', self.WIfS_general_cost3) # costG3
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_comment3', self.WIfS_comment3) # comment3

    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_name4', self.WIfS_name4)  # name4
    get_config_value_into_tkinter_label(self._Section_Name, 'WIfS_crop4', self.WIfS_crop4)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_crop_price4', self.WIfS_crop_price4) # price4
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert4', self.WIfS_cost_Nfert4) # costN4
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr4', self.WIfS_cost_irr4) # costi4
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_general_cost4', self.WIfS_general_cost4) # costG4
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_comment4', self.WIfS_comment4) # comment4

    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_name5', self.WIfS_name5)  # name5
    get_config_value_into_tkinter_label(self._Section_Name, 'WIfS_crop5', self.WIfS_crop5)
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_crop_price5', self.WIfS_crop_price5) # price5
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert5', self.WIfS_cost_Nfert5) # costN5
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr5', self.WIfS_cost_irr5) # costi5
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_general_cost5', self.WIfS_general_cost5) # costG5
    get_config_value_into_pmw_entryfield(self._Section_Name, 'WIfS_comment5', self.WIfS_comment5) # comment5

  def saveConfig(self):
    # Working directory
    set_config_value_from_string(self._Section_Name, 'Working_directory', self.Working_directory) # Wdir_path

    # # Threshold for water stress index
    # set_config_value_from_pmw_entryfield(self._Section_Name, 'WSI_threshold', self.WSI_threshold)   # wsi_threshold

    # What-If scenarios
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_name1', self.WIfS_name1)  # name1
    set_config_value_from_tkinter_label(self._Section_Name, 'WIfS_crop1', self.WIfS_crop1)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_crop_price1', self.WIfS_crop_price1) # price1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert1', self.WIfS_cost_Nfert1) # costN1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr1', self.WIfS_cost_irr1) # costi1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_general_cost1', self.WIfS_general_cost1) # costG1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_comment1', self.WIfS_comment1) # comment1

    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_name2', self.WIfS_name2)  # name2
    set_config_value_from_tkinter_label(self._Section_Name, 'WIfS_crop2', self.WIfS_crop2)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_crop_price2', self.WIfS_crop_price2) # price2
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert2', self.WIfS_cost_Nfert2) # costN2
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr2', self.WIfS_cost_irr2) # costi2
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_general_cost2', self.WIfS_general_cost2) # costG2
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_comment2', self.WIfS_comment2) # comment2

    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_name3', self.WIfS_name3)  # name3
    set_config_value_from_tkinter_label(self._Section_Name, 'WIfS_crop3', self.WIfS_crop3)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_crop_price3', self.WIfS_crop_price3) # price3
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert3', self.WIfS_cost_Nfert3) # costN3
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr3', self.WIfS_cost_irr3) # costi3
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_general_cost3', self.WIfS_general_cost3) # costG3
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_comment3', self.WIfS_comment3) # comment3

    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_name4', self.WIfS_name4)  # name4
    set_config_value_from_tkinter_label(self._Section_Name, 'WIfS_crop4', self.WIfS_crop4)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_crop_price4', self.WIfS_crop_price4) # price4
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert4', self.WIfS_cost_Nfert4) # costN4
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr4', self.WIfS_cost_irr4) # costi4
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_general_cost4', self.WIfS_general_cost4) # costG4
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_comment4', self.WIfS_comment4) # comment4

    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_name5', self.WIfS_name5)  # name5
    set_config_value_from_tkinter_label(self._Section_Name, 'WIfS_crop5', self.WIfS_crop5)
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_crop_price5', self.WIfS_crop_price5) # price5
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_Nfert5', self.WIfS_cost_Nfert5) # costN5
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_cost_irr5', self.WIfS_cost_irr5) # costi5
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_general_cost5', self.WIfS_general_cost5) # costG5
    set_config_value_from_pmw_entryfield(self._Section_Name, 'WIfS_comment5', self.WIfS_comment5) # comment5