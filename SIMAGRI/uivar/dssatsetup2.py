# -*- coding: utf-8 -*-
"""
Modified by Eunjin Han @IRI on Feb, 2020
======================================
# Redesigned on Thu December 26 17:28:23 2016
# @Re-designer: Seongkyu Lee, APEC Climate Center

##Program: CAMDT (Climate Agriculture Modeling Decision Tool)
##  The CAMDT is a computer desktop tool designed to guide decision-makers
##  in adopting appropriate crop and water management practices
##  that can improve crop yields given a climate condition
##Author: Eunjin Han
##Institute: IRI-Columbia University, NY
##Revised: August, 2, 2016
##Date: February 17, 2016
##
##Redesigned: December 26, 2016 (by Seongkyu Lee, APEC Climate Center)
##
##===================================================================
"""

from SIMAGRI._configuration import *

class UIVarDSSATSetup2():

  _Section_Name = 'DSSATSetup2'

  # Fertilization application
  rbFertApp = None # Fbutton1
  
  FA_nfertilizer = None # nfertilizer

  # 1st application
  FA_day1 = None # day1
  FA_amount1 = None  # amount1
  FA_fert_matl1 = None  # fert_mat1
  FA_fert_app_method1 = None  # fert_app1
  # 2nd application
  FA_day2 = None 
  FA_amount2 = None  
  FA_fert_matl2 = None 
  FA_fert_app_method2 = None 
  # 3rd application
  FA_day3 = None 
  FA_amount3 = None  
  FA_fert_matl3 = None  
  FA_fert_app_method3 = None  

  # Irrigation
  rbIrrigation = None # IRbutton
  irr_method = None  #irrigation method, entryfield

  def __init__(self):
    pass

  def initVars(self):
    pass

  # load from configuration file
  def loadConfig(self):
    # Fertilization application
    get_config_value_into_tkinter_intvar(self._Section_Name, 'rbFertApp', self.rbFertApp)  # Fbutton1

    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_nfertilizer', self.FA_nfertilizer) # nfertilizer

    # 1st application
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_day1', self.FA_day1)  # day1
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amount1', self.FA_amount1)  # amount1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_mat1', self.FA_fert_matl1) # fert_mat1
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_app_method1', self.FA_fert_app_method1) # fert_app1
    # 2nd application
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_day2', self.FA_day2)  # day2
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amount2', self.FA_amount2)  # amount2
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_mat2', self.FA_fert_matl2)  # fert_mat2
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_app_method2', self.FA_fert_app_method2)  # fert_app2
    # 3rd application
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_day3', self.FA_day3)  # day3
    get_config_value_into_pmw_entryfield(self._Section_Name, 'FA_amount3', self.FA_amount3)  # amount3
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_mat3', self.FA_fert_matl3)  # fert_mat3
    get_config_value_into_pmw_combobox(self._Section_Name, 'FA_fert_app_method3', self.FA_fert_app_method3)  # fert_app3

    # Irrigation
    get_config_value_into_tkinter_intvar(self._Section_Name, 'rbIrrigation', self.rbIrrigation)  # IRbutton
    get_config_value_into_pmw_combobox(self._Section_Name, 'irr_method', self.irr_method)  # automatic irrigation method
    
  # save into configration file
  def saveConfig(self):
    # Fertilization application
    set_config_value_from_tkinter_intvar(self._Section_Name, 'rbFertApp', self.rbFertApp)  # Fbutton1

    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_nfertilizer', self.FA_nfertilizer)  # nfertilizer

    # 1st application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_day1', self.FA_day1)  # day1
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amount1', self.FA_amount1)  # amount1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_mat1', self.FA_fert_matl1)  # fert_mat1
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_app_method1', self.FA_fert_app_method1)  # fert_app1
    # 2nd application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_day2', self.FA_day2)  # day2
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amount2', self.FA_amount2)  # amount2
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_mat2', self.FA_fert_matl2)  # fert_mat2
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_app_method2', self.FA_fert_app_method2)  # fert_app2
    # 3rd application
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_day3', self.FA_day3)  # day3
    set_config_value_from_pmw_entryfield(self._Section_Name, 'FA_amount3', self.FA_amount3)  # amount3
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_mat3', self.FA_fert_matl3)  # fert_mat3
    set_config_value_from_pmw_combobox(self._Section_Name, 'FA_fert_app_method3', self.FA_fert_app_method3)  # fert_app3

    # Irrigation
    set_config_value_from_tkinter_intvar(self._Section_Name, 'rbIrrigation', self.rbIrrigation)  # IRbutton
    set_config_value_from_pmw_combobox(self._Section_Name, 'irr_method', self.irr_method)  # automatic irrigation method
    